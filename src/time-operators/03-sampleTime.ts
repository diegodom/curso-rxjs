import { fromEvent, map, sampleTime } from "rxjs";

const click$ = fromEvent<PointerEvent>(document, "click");

interface Coords {
  x: number;
  y: number;
}

click$
  .pipe(
    sampleTime(2000),
    map<PointerEvent, Coords>(({ x, y }) => ({
      x,
      y,
    }))
  )
  .subscribe(console.log);
