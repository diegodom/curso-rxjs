import { auditTime, fromEvent, map, tap } from "rxjs";

const click$ = fromEvent<PointerEvent>(document, "click");

interface Coords {
  x: number;
  y: number;
}

click$
  .pipe(
    map<PointerEvent, Coords>(({ x, y }) => ({
      x,
      y,
    })),
    tap((v) => console.log("tap: ", v)),
    auditTime(2000)
  )
  .subscribe(console.log);
