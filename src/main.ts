import { ajax } from "rxjs/ajax";

const url = "https://httpbin.org/delay/1";

ajax
  .post(
    url,
    {
      id: 1,
      name: "Diego",
    },
    {
      JWT: "Aslkja29aa1a2AS",
    }
  )
  .subscribe(console.log);
