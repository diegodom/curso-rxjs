import { of, from } from "rxjs";

/**
 * of = toma argumentos y genera una secuencia de valores
 * from = array, obj, promise, iterable, observable
 */

const observer = {
  next: (val: any) => console.log("next: ", val),
  complete: () => console.log("Observer completed"),
};

const myGenerator = function* () {
  yield 1;
  yield 2;
  yield 3;
  yield 4;
  yield 5;
};

const myIterable = myGenerator();

/* for (let i of myIterable) {
  console.log(i);
} */

from(myIterable).subscribe(observer);

// const source$ = from([1, 2, 3, 4, 5]);
// const source$ = of(...[1, 2, 3, 4, 5]);

// const source$ = from("Diego");

const source$ = from(fetch("https://api.github.com/users/DiegoDom"));

// source$.subscribe(observer);

/* source$.subscribe(async (resp) => {
  const data = await resp.json();
  console.log(data);
}); */
