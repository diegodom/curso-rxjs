import { fromEvent } from "rxjs";

/**
 *?  Eventos del DOM
 */

const src1$ = fromEvent<MouseEvent>(document, "click");
const src2$ = fromEvent<KeyboardEvent>(document, "keyup");

src1$.subscribe({
  next: ({ x, y }) => {
    console.log("x", x);
    console.log("y", y);
  },
});
src2$.subscribe({
  next: (v) => console.log(v.key),
});
