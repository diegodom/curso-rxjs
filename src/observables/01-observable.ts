import { Observable, Observer } from 'rxjs'

const observer: Observer<any> = {
  next: (value) => console.log('next', value),
  error: (error) => console.error(error),
  complete: () => console.info('Observer completed'),
}

const obs$ = new Observable<string>((subs) => {
  subs.next('Hello')
  subs.next('World')

  subs.next('Hello')
  subs.next('World')

  subs.complete()
})

obs$.subscribe(observer)

/* obs$.subscribe({
  next: (value) => {
    console.log('next', value)
  },
  error: (err) => {
    console.error(err)
  },
  complete: () => {
    console.log('Completed')
  },
}) */
