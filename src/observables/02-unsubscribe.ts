import { Observable, Observer } from 'rxjs'

const observer: Observer<any> = {
  next: (value) => console.log('next', value),
  error: (error) => console.error(error),
  complete: () => console.info('Observer completed'),
}

const invervalo$ = new Observable<number>((subscriber) => {
  let n = 1

  const interval = setInterval(() => {
    subscriber.next(n++)
    console.log(n)
  }, 1000)

  setTimeout(() => {
    subscriber.complete()
  }, 2500)

  return () => {
    clearInterval(interval), console.warn('Intervalo destruido')
  }
})

const subs1 = invervalo$.subscribe(observer)
const subs2 = invervalo$.subscribe(observer)
const subs3 = invervalo$.subscribe(observer)

setTimeout(() => {
  subs1.unsubscribe()
  subs2.unsubscribe()
  subs3.unsubscribe()

  console.log('Completado timeout')
}, 5000)
