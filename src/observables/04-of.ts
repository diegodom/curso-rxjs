import { of } from "rxjs";

// const obs$ = of(1, 2, 3, 4, 5, 6);
const obs$ = of([1, 2], { a: 1, b: 2 }, function () {}, true, Promise.resolve(true));

console.log("Inicio del obs$");
obs$.subscribe({
  next: (n) => console.log("next", n),
  complete: () => console.log("Finished"),
});

console.log("Fin del obs$");
