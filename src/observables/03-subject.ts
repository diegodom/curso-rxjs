import { Observable, Observer, Subject } from 'rxjs'

const observer: Observer<any> = {
  next: (value) => console.log('next', value),
  error: (error) => console.error(error),
  complete: () => console.info('Observer completed'),
}

const intervalo$ = new Observable<number>((subs) => {
  const randInterval = setInterval(() => subs.next(Math.random() * 10), 1000)

  return () => {
    clearInterval(randInterval)
    console.log('Intervalo limpiado')
  }
})

/**
 * 1.- Castemo múltiple
 * 2.- También es un observer
 * 3.- Next, error & complete
 */

const subject$ = new Subject()

const intervalSubject = intervalo$.subscribe(subject$)

// const subs1 = intervalo$.subscribe((rnd) => console.log('subs1 ', rnd))
// const subs2 = intervalo$.subscribe((rnd) => console.log('subs2 ', rnd))

const subs1 = subject$.subscribe(observer)
const subs2 = subject$.subscribe(observer)

setTimeout(() => {
  subject$.next(10)

  subject$.complete()

  intervalSubject.unsubscribe()
}, 3500)
