import { interval, timer } from "rxjs";

const observer = {
  next: (v: any) => console.log("next: ", v),
  complete: () => console.log("Finished"),
};

const todayInFive = new Date(); // now
todayInFive.setSeconds(todayInFive.getSeconds() + 5);

const interval$ = interval(1000);
// const timer$ = timer(2000);
//const timer$ = timer(2000, 1000);
const timer$ = timer(todayInFive);

console.log("init");
// interval$.subscribe(observer);
timer$.subscribe(observer);
console.log("end");
