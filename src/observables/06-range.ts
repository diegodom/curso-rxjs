import { asyncScheduler, of, range } from "rxjs";

// const src$ = of(1, 2, 3, 4, 5, 6, 7, 9, 10);
const src$ = range(1, 10, asyncScheduler);

console.log("init");
src$.subscribe(console.log);
console.log("end");
