import { asyncScheduler } from "rxjs";

// setTimeout(() => {}, 3000);
// setInterval(() => {}, 3000);

const sayHi = () => console.log("Hello world");
const sayHiName = (name: any) => console.log(`Hello ${name}`);

// asyncScheduler.schedule(sayHi, 3000);
// asyncScheduler.schedule(sayHiName, 2000, "DOM");

const subs$ = asyncScheduler.schedule(
  function (state) {
    console.log("state", state);

    this.schedule(state! + 1, 1000);
  },
  3000,
  0
);

/* setTimeout(() => {
  subs$.unsubscribe();
}, 6000); */

asyncScheduler.schedule(() => subs$.unsubscribe(), 6000);
