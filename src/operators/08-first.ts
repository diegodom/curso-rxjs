import { fromEvent } from "rxjs";
import { first, map, tap } from "rxjs/operators";

const click$ = fromEvent<PointerEvent>(document, "click");

click$
  .pipe(
    tap(({ clientY }) => console.log(clientY)),
    map<MouseEvent, { y: number; x: number }>(({ clientX, clientY }) => ({
      y: clientY,
      x: clientX,
    })),
    first(({ y }) => y >= 150)
  )
  .subscribe({
    next: (e) => console.log("next: ", e),
    complete: () => console.log("Completed"),
  });
