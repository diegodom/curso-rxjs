import { fromEvent, interval } from "rxjs";
import { skip, takeUntil } from "rxjs/operators";

const button = document.createElement("button");
button.innerHTML = "Stop Timer";

document.querySelector("body")?.append(button);

const counter$ = interval(1000);
// const btnClick$ = fromEvent(button, "click");
const btnClick$ = fromEvent(button, "click").pipe(skip(1));

counter$.pipe(takeUntil(btnClick$)).subscribe({
  next: (n) => console.log("Next: ", n),
  complete: () => console.log("Completed"),
});
