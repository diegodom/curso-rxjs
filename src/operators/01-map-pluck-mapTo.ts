import { fromEvent, range } from "rxjs";
import { map, mapTo, pluck } from "rxjs/operators";

/* range(1, 5)
  .pipe(map<number, number>((v) => v * 10))
  .subscribe(console.log);
 */

const keyup$ = fromEvent<KeyboardEvent>(document, "keyup");

const keyupMap$ = keyup$.pipe(map((v) => v.code));
const keyupPluck$ = keyup$.pipe(pluck("key"));
const keyupMapTo$ = keyup$.pipe(mapTo("tecla presionada"));

keyupMap$.subscribe((v) => console.log("map ", v));
keyupPluck$.subscribe((v) => console.log("pluck ", v));
keyupMapTo$.subscribe((v) => console.log("MapTo ", v));
