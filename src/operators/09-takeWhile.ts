import { fromEvent } from "rxjs";
import { map, takeWhile } from "rxjs/operators";

const click$ = fromEvent<PointerEvent>(document, "click");

interface Coords {
  x: number;
  y: number;
}

click$
  .pipe(
    map<PointerEvent, Coords>(({ x, y }) => ({ x, y })),
    // takeWhile(({ y }) => y <= 150)
    takeWhile(({ y }) => y <= 150, true)
  )
  .subscribe({
    next: (e) => console.log("next: ", e),
    complete: () => console.log("Completed"),
  });
