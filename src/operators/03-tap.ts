import { range } from "rxjs";
import { map, tap } from "rxjs/operators";

const numbers$ = range(1, 5);

numbers$
  .pipe(
    tap((x) => console.log("before", x)),
    map((n) => n * 10),
    tap({
      next: (n) => console.log("after", n),
      complete: () => console.log("after tab completed"),
    })
  )
  .subscribe((n) => console.log("result", n));
