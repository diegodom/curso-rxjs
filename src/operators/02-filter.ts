import { from, fromEvent, range } from "rxjs";
import { filter, map } from "rxjs/operators";

/* range(1, 10)
  .pipe(filter((v) => v % 2 === 1))
  .subscribe(console.log); */

interface Character {
  type: string;
  name: string;
}

const characters: Character[] = [
  {
    type: "hero",
    name: "Batman",
  },
  {
    type: "hero",
    name: "Robin",
  },
  {
    type: "villian",
    name: "Joker",
  },
];

from(characters)
  .pipe(filter((character) => character.type === "hero"))
  .subscribe(console.log);

const keyup$ = fromEvent<KeyboardEvent>(document, "keyup").pipe(
  map((event) => event.code),
  filter((code) => code === "Enter")
);

keyup$.subscribe(console.log);
