import { from } from "rxjs";
import { map, reduce, scan } from "rxjs/operators";

const numbers = [1, 2, 3, 4, 5];

const reducerTotal = (acc: number, cur: number) => acc + cur;

//? Reduce
from(numbers).pipe(reduce(reducerTotal, 0)).subscribe(console.log);

//? Scan
from(numbers).pipe(scan(reducerTotal, 0)).subscribe(console.log);

//? Redux
interface User {
  id?: string;
  isAuth?: boolean;
  jwt?: string | null;
  name?: string;
}

const users: User[] = [
  {
    id: "aaASA1",
    isAuth: false,
    jwt: null,
  },
  {
    id: "aaASA1",
    isAuth: true,
    jwt: "alsjakhbabjsk",
  },
  {
    id: "aaASA1",
    isAuth: true,
    jwt: "cncaamsjhdsa1",
  },
];

const state$ = from(users).pipe(
  scan<User>((acc: User, cur: User) => {
    return { ...acc, ...cur };
  })
);

const id$ = state$.pipe(map((state) => state));
id$.subscribe(console.log);
