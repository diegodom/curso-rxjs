import { distinctUntilChanged, from, of } from "rxjs";

const numbers$ = of(1, "1", 1, 3, 3, 2, 2, 4, 4, 5, 3, 1);

numbers$
  .pipe(
    distinctUntilChanged() // ===
  )
  .subscribe(console.log);

interface Character {
  name: string;
}

const characters: Character[] = [
  {
    name: "Luffy",
  },
  {
    name: "Luffy",
  },
  {
    name: "Zoro",
  },

  {
    name: "Brook",
  },
  {
    name: "Sanji",
  },
  {
    name: "Luffy",
  },
  {
    name: "Zoro",
  },
  {
    name: "Zoro",
  },
  {
    name: "Brook",
  },
];

from(characters)
  .pipe(distinctUntilChanged((prev, cur) => prev.name === cur.name))
  .subscribe(console.log);
