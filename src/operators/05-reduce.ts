import { interval } from "rxjs";
import { reduce, take, tap } from "rxjs/operators";

const numbers = [1, 2, 3, 4, 5];

const reducerTotal = (acumulator: number, currentValue: number) => {
  return acumulator + currentValue;
};

const total = numbers.reduce(reducerTotal, 0);

console.log("total arr", total);

interval(1000)
  .pipe(take(3), tap(console.log), reduce(reducerTotal, 0))
  .subscribe({
    next: (v) => console.log("next: ", v),
    complete: () => console.log("completed"),
  });
