import { of } from "rxjs";
import { take, tap } from "rxjs/operators";

const numbers$ = of(1, 2, 3, 4, 5);

numbers$
  .pipe(
    tap((t) => console.log("tap: ", t)),
    take(2)
  )
  .subscribe({
    next: (n) => console.log("next: ", n),
    complete: () => console.log("Completed"),
  });
