import { distinctUntilKeyChanged, from } from "rxjs";

interface Character {
  name: string;
}

const characters: Character[] = [
  {
    name: "Luffy",
  },
  {
    name: "Luffy",
  },
  {
    name: "Zoro",
  },
  {
    name: "Brook",
  },
  {
    name: "Sanji",
  },
  {
    name: "Luffy",
  },
  {
    name: "Zoro",
  },
  {
    name: "Zoro",
  },
  {
    name: "Brook",
  },
];

from(characters).pipe(distinctUntilKeyChanged("name")).subscribe(console.log);
