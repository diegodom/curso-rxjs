import { of } from "rxjs";
import { AjaxError, ajax } from "rxjs/ajax";
import { catchError, map } from "rxjs/operators";

const url = "https://api.github.com/users?per_page=5";

const errorHandler = async (response: Response) => {
  const { ok, statusText } = response;

  if (!ok) {
    throw new Error(`Ocurrio un error al intentar hacer la petición. CODE: ${statusText}`);
  }

  return await response.json();
};

const errorHandlerAjax = (err: AjaxError) => {
  console.warn("Error into: ", err.message);
  return of([]);
};

const fetchPromise = fetch(url);

/* fetchPromise
  .then((resp) => resp.json())
  .then(console.log)
  .catch(console.error); */

/* fetchPromise.then(errorHandler).then(console.log).catch(console.error); */

ajax(url)
  .pipe(
    map((data) => data.response),
    catchError(errorHandlerAjax)
  )
  .subscribe(console.log);
