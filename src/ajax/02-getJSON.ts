import { ajax } from "rxjs/ajax";

const url = "https://httpbin.org/delay/1";

const obs$ = ajax.getJSON(url, {
  "Content-type": "application/json",
  jwt: "wasjqwok2182aAZaSLK1",
});

obs$.subscribe(console.log);
