import { of } from "rxjs";
import { AjaxError, ajax } from "rxjs/ajax";
import { catchError } from "rxjs/operators";

const url = "https://httpbiaadn.org/delay/1";

const errorHandler = (err: AjaxError) => {
  console.warn("Error into: ", err.message);
  return of([]);
};

/* const obs$ = ajax.getJSON(url).pipe(catchError(errorHandler));
const obs2$ = ajax(url).pipe(catchError(errorHandler)); */

const obs$ = ajax.getJSON(url);
const obs2$ = ajax(url);

obs$.pipe(catchError(errorHandler)).subscribe({
  next: (data) => console.log(data),
  error: (err) => console.warn("err", err),
});
